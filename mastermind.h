//MASTERMIND NAGLOWEK

//PROTOTYPY FUNKCJI
#ifndef MASTERMIND
#define MASTERMIND

//FUNKCJE POMOCNICZE
void swap(int *a, int *b);
int powt(int *tab,int x);
int check(int odp[],int tab[],int x);	
void czysc(int *tab,int **odp,int l);
int przelicz(int i, int j,int x);
void zeruj(int *tab,int x);

//FUNKCJE POBIERAJACE
int getintx(int y);
int getodpx(int y);
int getzak(int a);
int getint(void);
int getlos(void);	
int * getodp(int x, int y);
void pobierzpoz(int *x,int y);

//FUNKCJE LOSUJACE
void losuj1(int *tab,int n,int y);
void losuj2(int *tab,int n,int y);
void losuj3(int *tab,int n,int y);
void losuj4(int *tab,int n,int y);
void losuj4x(int *tab,int l,int u,int *i);

//FUNCKJE UZYTKOWE
void losowanie(int *los,int n,int y);
void poziom(int **tab,int x);
void zakres(int x, int *y);
int pobierz(int *los,int x, int y,int **hist,int licznik);
void pomoc(void);
void komputer(int x,int y);
void pokazhist(int *hist,int licznik,int x);
#endif