// GRA MASTER-MIND PLIK Z FUNKCJAMI
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<math.h>
#include"mastermind.h"
						/*SPOSOB ZAPISU PONIZSZYCH FUNKCJI INLINE MOZE BYC ROZNY DLA INNYCH SYSTEMOW!!!!*/

__inline void sprawdz(void * a)										//FUNKCJA SPRAWDZA CZY ALOKOWANIE PAMIECI SIE POWIODLO
{
	if(a==NULL)
	{
				fprintf(stderr,"Blad alokacji pamieci");
				exit(1);
	}
return;
}

__inline void usun(void)												//FUNKCJA CZYSZCZACA STRUMIEN WEJSCIA
{
	while(getchar()!='\n')
		continue;
}

int getintx(int y)														//FUNKCJA POBIERA ILOSC LICZB DO WYLOSOWNAIA(LICZBE Z PRZEDZIALU (0;y-2))
{
	int i;
			while( (scanf("%d",&i))!=1 || !(i>0 && i<y-2) )
			{
				usun();
				printf("Zly wybor sproboj jeszcze raz!!\n");
			}
			usun();
	return i;
}

int getodpx(int y)													//FUNKCJA POBIERA np.LICZBE I PRZESYLA DO ODPOWIEDZI (LICZBE Z PRZEDZIALU <0;y))
{
	int i;
	
			while( (scanf("%d",&i))!=1 || !(i>=0 && i<y))
			{
				usun();
				printf("Zly wybor sproboj jeszcze raz!!\n");
			}
			usun();
	return i;
}		

int getzak(int a)											//FUNKCJA POBIERAJACA ZAKRES(LICZBE Z PRZEDZALU (a+2;+niesk.))
{
	int i;
	while( (scanf("%d",&i))!=1 || i<a+2)
	{
		usun();
		printf("Zly wybor sproboj jeszcze raz!!\n");
	}
	usun();
	return i;
}

int getint(void)											//FUNKCJA POBIERAJACA INTIGIERA
{
	int i;
	while( (scanf("%d",&i))!=1)
	{
		usun();
		printf("Zly wybor sproboj jeszcze raz!!\n");
	}
	usun();
	return i;
}

int getlos(void)													//FUNKCJA POBIERAJACA np.NUMER TYPU LOSOWANIA(LUCZBE Z PRZEDZIALU <1;4>)
{
	int i;
	
		while( (scanf("%d",&i))!=1 || !(i>=1 && i<=4))
			{
				usun();
				printf("Zly wybor sproboj jeszcze raz!!\n");
			}
	usun();
	return i;
}
int * getodp(int x, int y)								//FUNKCJA PRZYDZIELA PAMIEC NA ODPOWIEDZ I ZAPISUJE JA
{														//KORYGUJE TAKZE BLEDY POWTORZENIA
	int *odp= (int *) malloc(sizeof(int)*x);
	int i, j;

		sprawdz(odp);
		
		for(i=0;i<x;)
			{
					odp[i]=getodpx(y);
						for(j=0;j<i;j++)
							if(odp[j]==odp[i])
							break;
					if(j==i)
						i++;
					else
						printf("Wartosci nie mogo sie powtarzac!!\n");
			}
	return odp;
}

void swap(int *a, int *b)											//FUNKCJA ZAMIENIAJACA MIEJSCAMI 2 INTIGERY W TABLICY
{
	int temp;
	temp=*a; *a=*b; *b=temp;
}
	
void losuj1(int *tab,int n,int y)										//LOSOWANIE ZWYKLE
{
		int i=0, j;
		srand(time(0));
		
			while(i<n)
			{
				tab[i]=rand()%y;
					for(j=0;j<i;j++)
						if(tab[j]==tab[i])
						break;
				if(j==i)
				i++;
			}
}
void losuj2(int *tab,int n,int y)										//LOSOWANIE PRZEZ MIESZANIE ELEMENTOW TABLICY
{
	int *tap;
	int i;
		
		tap= (int *) malloc(sizeof(int)*y);
			sprawdz(tap);
		
		for(i=0;i<y;i++)
			tap[i]=i;
			
		srand(time(0));
		for(i=0;i<n;i++)
			swap(&tap[i],&tap[rand()%y]);
		
		for(i=0;i<n;i++)
			tab[i]=tap[i];
	
		free(tap);
}
	
void losuj3(int *tab,int n,int y)												//LOSOWANIE PRZEZ WYBOR ELEMENTOW Z TABLICY
{
	int *tap;
	int i, j=0;
	
		tap= (int *) malloc(sizeof(int)*y);
			sprawdz(tap);
		
		for(i=0;i<y;i++)
			tap[i]=i;
	
	srand(time(0));
	for(i=0;i<y;i++)
		{
			if(rand()%(y-i)<n)
				{
					tab[j++]=i;
					n--;
				}
		}
	free(tap);
}

void losuj4(int *tab,int n,int y)							//LOSOWANIE REKURENCYJNE
{
int i=n;
		srand(time(0));
		losuj4x(tab,0,y-1,&i);		
}

void losuj4x(int *tab,int l,int u,int *i)
{
	int m;
	
	if(*i<=0 || l>u)
		return;
	
	
	m=rand()%(u-l+1) + l;
	
	tab[*i-1]=m;
	(*i)--;
	losuj4x(tab,l,m-1,i);
	losuj4x(tab,m+1,u,i);
}

void losowanie(int *los,int n,int y)									//FUNCKJA LOSOWANIE WYBIERA TYP LOSOWANIA I LOSUJE LICZBY ZAPISUJAC
{																		//JE DO TABLICY los
	int x;
		
		puts("W Jaki sposob chcesz wylosowac liczby");
		printf("1) Standardowy\n");
		printf("2) Mieszanie tablicy\n");
		printf("3) Wybor elementow tablicy\n");
		printf("4) Losowanie rekurencyjne\n");
		
		x=getlos();
		switch(x)
		{
			case 1:
				losuj1(los,n,y);
				break;
			case 2:
				losuj2(los,n,y);
				break;
			case 3:
				losuj3(los,n,y);
				break;
			case 4:
				losuj4(los,n,y);
		}
	printf("Tak wiec musisz odgadnac %d liczb\n",n);
	printf("liczby beda losowane z przedzialu od 0 do %d \n",(y)-1);
}
		
void poziom(int **tab,int x)														//FUNKCJA ALOKUJE PAMIEC DLA ODPOWIEDZI
{
	*tab=(int *) malloc(sizeof(int)* (x));
	
		sprawdz(*tab);
}

void pobierzpoz(int *x,int y)										//FUNKCJA ZAPISUJE ILOSC LICZB DO WYLOSOWANIA DO ZMIENNEJ
{
	printf("Podaj ilosc liczb do odgadniecia\n");
	printf("liczba ta musi byc nieujemna i co najmniej o 2 mniejsza od poziomu\n");
	*x=getintx(y);
}


void zakres(int x, int *y)																//FUNKCJA ZAKRES WYBIERA ZAKRES LICZB DO ZGADYWANIA
{
	printf("Podaj zakres z jakiego beda losowane liczby\n");
	printf("Zakres musi byc co najmniej o 2 wiekszy od poziomu\n");
	printf("trudnosci: ");
	*y=getzak(x);
}

int pobierz(int *los,int x, int y,int **hist,int licznik)
{
	int *odp;										//odp - odpowiedz uzytkownika
	int a, b;										//a - liczba  cyferek na dobrym miejscu b - liczba cyferek dobrych na zlym miejscu
	int i,j;
	
	a=b=0;

					*hist= (int *) realloc(*hist ,((x+2)*licznik)*sizeof(int) );
					sprawdz(*hist);
	
			printf("Podaj swoja odpowiedz:\n");
			printf("Kolejne liczby oddzielaj enterem\n");
				odp=getodp(x,y);
				
	
			
		for(i=0;i<x;i++)
				if(los[i]==((*hist)[przelicz(licznik-1, i,x)]=odp[i])) 
					a++;
		
		if(a==x)
		{	
			free(hist);
			return 0;
		}
			
		for(j=0;j<x;j++)
			for(i=0;i<x;i++)
				if(i==j)
					continue;
				else if(los[j]==odp[i])
					b++;
					
	printf("Twoja odpowiedz: \n");
	for(i=0;i<x;i++)
	printf("%d ",odp[i]);
	printf("\nLiczba liczb na dobrym miejscu: %d | Liczba liczb dobrych ale na zlym miejscu: %d\n"
			,(*hist)[przelicz(licznik-1, x,x)]=a,(*hist)[przelicz(licznik-1, x+1,x)]=b);
		
	free(odp);
	return 1;
}

void pomoc(void)												//WYSWIETLENIE POMOCY
{
	printf("Mastermind to gra ktora polega na zgadywaniu liczb wymyslonych przez komputer\n");
	printf("na podstawie jego podpowiedzi\n");
	printf("Im szybciej zgadniesz wylosowana liczbe tym jestes fajniejszy\n");
	printf("Wejscie w menu ustawienia umozliwia zmiane liczby wylosowanych liczb\n");
	printf("i zakresu z ktorego sa losowane tym samych czyniac gre trudniejsza\nlub latwiejsza\n");
	printf("Gra posiada takze zabawna opcje gry z komputerem ktory zgaduje liczbe wymyslona\n");
	printf("przez gracza poprzez dawanie mu podpowiedzi\n");
	printf("POWODZENIA\n");
}

int powt(int *tab,int x)										//FUNKCJA SPRAWDZA POWTORZENIA W TABLICY
{
	int i,j;
		for(i=0;i<x;i++)
			for(j=i+1;j<x;j++)
				if(tab[j]==tab[i])
					return 1;
	return 0;
}

int check(int odp[],int tab[],int x)						//FUNKCJA SPRAWDZA ZGODNOSC Z POPRZEDNIMI ODPOWIEDZIAMI
{
	int a,b;
	int j,k;
	
				a=b=0;
				for(k=0;k<x;k++)
					for(j=0;j<x;j++)
							if(odp[k]==tab[j])
								{
									a++;
									if(k==j)
									b++;
								}
		if(a!=odp[x] || b!=odp[x+1])
			return 0;
		
		return 1;
}

void komputer(int x,int y)									//FUNKCJA OBSLUGUJACE GRE GRACZ VS KOMPUTER
{
	int *tab;
	int **odp=NULL;
	int i,j,k,l,flag,licznik,o;
	
				tab=(int *) calloc(x,sizeof(int));
				sprawdz(tab);
		printf("Wymysl jakas kombinacje %d liczb z przedzialu <0,%d>\n",x,y-1);
		l=-1; licznik=0;
		losuj1(tab,x,y);
	while(1)
	{	
		o=licznik;
		while(tab[0]<y)								//PETLA PRZECHODZI PO WSZYSTKICH WARIACJACH Z POWTORZENIAMI
			{
			
				if(!(powt(tab,x)) && (l==-1 || flag==1))					//WARUNEK SPRAWDZAJACY POWTORZENIA I ZGODNOSC Z ODPOWIEDZIAMI
				{
					l++;
						odp=(int **) realloc(odp,sizeof(int *) * (l+1));
						sprawdz(odp);
						odp[l]= (int *) malloc( (x+2)*sizeof(int) );
						sprawdz(odp[l]);
						
					for(k=0;k<x;k++)
						{
							printf("%d ",tab[k]);
							odp[l][k]=tab[k];
						}
							putchar('\n');
							printf("Podaj liczbe liczb ktore z znajduja sie w twojej wymyslonaj liczbie: ");
							odp[l][x]=getodpx(x+1);
							printf("A teraz podaj liczbe liczb ktore sposrod nich znajduja sie na dobrym miejscu: ");
							odp[l][x+1]=getodpx(odp[l][x]+1);
							licznik++;
				}			
					if(l!=-1 && odp[l][x+1]==x)
						{
							printf("Komputer zgadl w %d ruchach\n",licznik);
							czysc(tab,odp,l);
							return;
						}

			
				tab[x-1]++;													//TU NASTEPUJE PRZEJSCIE DO NASTEPNEJ LICZBY
		
				for(j=x-1;j>=1;j--)
						if(tab[j]==y)
						{
							tab[j]=0;
							tab[j-1]++;
						}
				flag=0;
				if(!(powt(tab,x)))	
					for(j=0,k=0;j<=l;j++)
						if((check(odp[j],tab,x)))
							k++;
				if(k>l)											//TU SPRAWDZANY JEST ZAKRES BLEDOW POPRAWNOSC KODU WZGLEDEM POZOSTALYCH
				flag=1;	
			}
			if(o==licznik)
			{
			printf("Oszukiwales\n");
			printf("Chcesz sie poprawic??\n");
			printf("1)Proboj dalej | 2)Koniec\n");
			k=getintx(5);
				switch(k)
				{
					case 1:
						free(odp[0]);
							for(i=0;i<l;i++)
								odp[i]=odp[i+1];
						losuj1(tab,x,y);
						l--;
						break;
					case 2:
							czysc(tab,odp,l);
							return;
				}
			}
			else
			zeruj(tab,x);
	}
}

void czysc(int *tab,int **odp,int l)						//FUNKCJA CZYSZCZACA PAMIEC
{
	int i;
	free(tab);
	for(i=0;i<=l;i++)
		free(odp[i]);
	free(odp);
}

void pokazhist(int *hist,int licznik,int x)
{
	int i,j;
		if(licznik==0)
			printf("Ni ma historii :(\n");
		for(i=0;i<licznik;i++)
			{
				for(j=0;j<x;j++)
					printf("%d ",hist[przelicz(i,j,x)]);
				printf("|%d dobre i %d na zlym miejscu|\n",hist[przelicz(i,j,x)],hist[przelicz(i,j+1,x)]);
			}
}

int przelicz(int i, int j,int x)
{
	return (i*(x+2)+j);
}

void zeruj(int *a, int x)
{
	int i;
		for(i=0;i<x;i++)
		a[i]=0;
}

