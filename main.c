//MASTERMIND GRA		KOMPILOWAC RAZEM Z PLIKIEM mastermind.c
#include<stdio.h>
#include"mastermind.h"
//#define TESTOFF				//GDY STALA JEST OBJETA KOMENTARZEM WINIK LOSOWANIA KOMPUTERA BEDZIE WIDOCZNY!!

int main(void)
{
	int *los;								//los - tablica przechowujaca losowe liczby do odgadniecia
	int x=4, y=7, i, menu, ust;				// x- poziom trudnosci , y - zakres liczb do losowania tzn. zakres wynosi <0,y) 
	int licznik;							// licznik - liczba prob odgadniecia
	int *hist=NULL;						//hist- tablica przechowujaca historie
	int flag, gra, nowa;
	
	printf("(ostatnia modyfikacja %s o godzinie %s)\n",__DATE__,__TIME__);
	do
	{
		printf("\t\t\tMASTERMIND\n");
		printf("1) NOWA GRA\n");
		printf("2) USTAWIENIA\n");
		printf("3) POMOC\n");
		printf("4) KONIEC\n");
			
			menu=getint();
			switch(menu)
			{
				case 1 :
					printf("1)KOMPUTER VS GRACZ | 2)GRACZ VS KOMPUTER\n");
					while((nowa=getint())<1 || nowa>2)
						printf("Zly wybor\n");
						
						switch(nowa)
						{
							case 1:
								licznik=0;
								poziom(&los,x);
								losowanie(los,x,y);
					
#ifndef TESTOFF
		for(i=0;i<x;i++)
			printf("%d ",los[i]);
			putchar('\n');
#endif				
								do
								{
									printf("1)Zgauj Liczbe | 2)Koniec | 3)Wyswietl historie\n");
									gra=getint();
									switch(gra)
									{
										case 1 :
											licznik++;
											flag=pobierz(los,x,y,&hist,licznik);
											break;
										case 2 :
											flag=2;
											break;
										case 3 :
											pokazhist(hist,licznik,x);
											break;
										default :
											printf("Nie ma takiej opcji\n");
											break;
									}
								}
								while(gra!=2 && flag);
			
								if(flag==0)
									printf("Gratulacje!!\n");
						
								if(flag==2)
									printf("Poddales sie!!\n");
						
								printf("Liczba prob: %d\n",licznik);
					
								free(los);
								break;
							case 2:
								komputer(x,y);
								break;
						}
					break;
				case 2 :
					do
					{
						printf("1) ZMIEN LICZBE LICZB DO ODGADNIECIA	(Obecna Watrosc: %d)\n",x);
						printf("2) ZMIEN ZAKRES LICZB DO LOSOWANIA	(Obecny Zakres: <0,%d>)\n",y-1);
						printf("3) POWROT DO MENU GLOWNEGO\n");
							ust=getlos();
								if(ust==1)
									pobierzpoz(&x,y);
								else if(ust==2)
									zakres(x,&y);
								else 
									printf("Zly wybor sproboj jeszcze raz\n");
					}
					while(ust!=3);
					break;
				case 3 :
					pomoc();
					break;
				case 4 :
					printf("KONIEC PROGRAMU :(\n");	
					break;
				default :
					printf("Nie ma takiej opcji\n");
					break;
			}
	}
	while(menu!=4);
	
return 0;
}